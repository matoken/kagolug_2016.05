#!/bin/bash

ASCIIDOC="asciidoc"
WKHTMLTOPDF="$HOME/usr/local/wkhtmltox/bin/wkhtmltopdf"
DOC=()
DOC=("Let's Encrypt更新話.adoc" "RaspberryPi_RealVNC.adoc" )

for (( I = 0; I < ${#DOC[@]}; ++I ))
do
  $ASCIIDOC --backend slidy "${DOC[$I]}"
  echo "mv ${DOC[$I]/\.adoc/\.html} ${DOC[$I]/\.adoc/_slide\.html}"
  mv "${DOC[$I]/\.adoc/\.html}" "${DOC[$I]/\.adoc/_slide\.html}"
  $ASCIIDOC --backend html5 "${DOC[$I]}"
  $WKHTMLTOPDF "${DOC[$I]/\.adoc/\.html}" "${DOC[$I]/\.adoc/\.pdf}"
done

# https://twitter.com/matoken/status/733509180313591808
gist-paste -u https://gist.github.com/matoken/c98ce0dd5baf30da604a40538b3944ae ./RaspberryPi_RealVNC_slide.html


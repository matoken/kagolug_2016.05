= Raspberry PiのSDL等に対応したRealVNCを試してみた
:author:    Kenichiro MATOHARA
:copyright: matoken
// :backend:   slidy
:max-width: 45em
:data-uri:
:presenter: Kenichiro MATOHARA
:twitter: matoken
:email: matoken@gmail.com
:web: http://matoken.org
:currentyear: 2016
// :backend: deckjs
:deckjsdir: ../../asciidoc/deck.js
:deckjs_transition: horizontal-slide
:deckjs_theme: neotech
:goto:
:menu:
:icons: font
:source-highlighter: codemirror
:navigation:
:status:
:arrows:
:customjs: ../../asciidoc/js/checkcypher.js
:gist-source: https://raw.github.com/neo4j-contrib/gists/master/
:footer: © All Rights Reserved {currentyear} | Neo Technology, Inc.
:logo: img/Neo_Technology.jpg
:allow-uri-read:
:img: image
:video:
:docs-link: https://github.com/neo4j-contrib/asciidoc-slides[documentation]
:download-link: https://github.com/neo4j-contrib/asciidoc-slides/archive/master.zip[download]
:sectids!:

include::about.adoc[]

== Raspberry PiのSDL等に対応したRealVNCを試してみた

== Raspberry Pi?

* Linux等が動くARMなシングルボードコンピュータ
* 幾つか種類がある
** デスクトップ向け Raspberry Pi 3 B(ARM64 4Core/1GB RAM)
** 電子工作向け Raspberry Pi 2 B+ / A+
** 組み込み向け Raspberry Pi Zero
** 産業向け Raspberry Pi Compute Module
* 類似品は沢山あるがRaspberry Piは情報量が多い
** Piでプロトタイプングして用途に合ったボードで実装というのがいい感じ
* 今日のネタはZeroでも初代でも3Bでもどれでも使えます :)

== 最近のRaspberry Piといえば
=== 5/16 17:00(JST)に Raspberry Pi Zero新バージョン発表&販売開始!

* 一番小さくて安いモデルでカメラが接続できるようになった! +
https://www.raspberrypi.org/blog/zero-grows-camera-connector/[Zero grows a camera connector - Raspberry Pi]

image::image/new_pizero.jpg["NEW PIZERO",align="ringh",width="640"]


== Raspberry Pi 公式blogにて

=== https://www.raspberrypi.org/blog/minecraft-pi-and-more-over-vnc/[Minecraft Pi (and more) over VNC - Raspberry Pi]

    RealVNC have released a free alpha (testing) version of VNC for Raspberry Pi that lets you remotely view and control everything on your Pi, including Minecraft, from a different computer. It works on every generation of Raspberry Pi, including Pi Zero.

* Minecraftとかがリモートで表示できるRealVNCの無料alpha版出したよ!全てのPiで動くよ!

== VNC?

* Virtual Network Computing(VNC)
* リモートのコンピュータのGUIでの遠隔操作が出来る
* マルチプラットホーム!
* WindowsのRDPと比較して遅い，帯域を食う，マルチメディアに弱い +
(LinuxでRDPが使えるプロジェクトも -> https://xrdp.vmeta.jp/[日本 xrdp ユーザ会] )
* 基本的に映像や音は送れない

== これまでのVNC接続

* Minecraftなどのゲーム，動画，カメラのプレビューなどは直にRAMに書き込むので真っ黒のウィンドウしか表示されなかった

image::image/VNC-black.png[]

== 今回

* こんな感じになるはず

image::RealVNC_raspi-preview_Preview_version_of_VNC_Server_optimized_for_the_Raspberry_Pi_files/MinecraftPi.gif[]

※画像は https://www.raspberrypi.org/blog/minecraft-pi-and-more-over-vnc/[Minecraft Pi (and more) over VNC - Raspberry Pi] より

== 導入手順

* Rasbian jessie or wheezy の環境を用意
* Raspberry Pi用RealVNC α1パッケージを入手して導入
* Raspberry Pi で RealVNC をサービスモードで起動
* 適当なマシンから接続
* 設定調整(option)

== 導入例

== Raspberry Pi用RealVNCパッケージを入手して導入

----
$ wget https://github.com/RealVNC/raspi-preview/releases/download/5.3.1.18206/VNC-Server-5.3.1-raspi-alpha1.deb
$ sha256sum VNC-Server-5.3.1-raspi-alpha1.deb
70fda7d2988abc5279b52a29f84c38bad5d094a3b47a995c005ee7181b06a07d VNC-Server-5.3.1-raspi-alpha1.deb
$ sudo dpkg -i VNC-Server-5.3.1-raspi-alpha1.deb
----

== Raspberry Pi で RealVNC をサービスモードで起動

== アドホックに起動

* systemdの場合
----
$ sudo systemctl start vncserver-x11-serviced.service
----

* sysvinitの場合
----
$ sudo /etc/init.d/vncserver-x11-serviced start
----

== 永続化(次回起動時も自動起動したい時)

* systemdの場合
----
$ sudo systemctl enable vncserver-x11-serviced.service
----

* sysvinitの場合
----
$ sudo update-rc.d vncserver-x11-serviced defaults
----

== 接続できない

=== Remmina 1.1.2-3(remmina-plugin-vnc 1.1.2-3)

image::image/remmina-ng.png[]

=== VNC Viewer Free Edition 4.1.1

----
$ vncviewer raspberrypi.local
VNC Viewer Free Edition 4.1.1 for X - built Aug 17 2015 11:14:27
Copyright (C) 2002-2005 RealVNC Ltd.
See http://www.realvnc.com for information on VNC.
Tue May 10 06:35:29 2016
CConn: connected to host ubuntu-mate.local port 5900
CConnection: Server supports RFB protocol version 5.0
CConnection: Using RFB protocol version 3.8
CConnection: No matching security types
main: No matching security types
----

== 最新のRealVNCを使う

=== 以下から入手

* https://www.realvnc.com/download/get/1868/[Download VNC® Viewer - RealVNC®]

=== 実行権を付けて実行

----
$ chmod +x VNC-Viewer-5.3.1-Linux-x64
$ ./VNC-Viewer-5.3.1-Linux-x64 raspberrypi.local
----

== 繋がったけど……

== minecraftの窓の中でなにか動いているけど……

image::image/vnc-noiz.png[]

== Windows版だとどうだろう?

image::image/vnc-win-noiz.png[]

== うまく行かない

* issueやtwitterを探すけど困っている人を見かけない……
* user modeだと動くけど従来と同じ黒い窓に
* Raspbian jessie lite(Xなしで小さめのパッケージ)を元にX等入れた環境なのでなにかおかしくなっているかも?

== Raspbian jesiseを導入しなおして再度

=== うまくいった!

image::image/vnc-ok.png[]

== RealVNC Viewerの推奨設定

=== Expertで

* PreferredEncoding=JPEG
* ColorLevel=full
* AutoSelect=False

== PreferredEncoding=JPEG
image::image/vnc-option03.png[]

== ColorLevel=full
image::image/vnc-option01.png[]

== AutoSelect=False
image::image/vnc-option02.png[]

== Raspberry Pi推奨設定

* RAM 128MB以上 +
raspi-confg or rc_gui で設定&再起動

** 9 Advanced Options
*** A3 Memory SplitA3 Memory SplitA3 Memory Split
**** 128MB

== こういう時に便利?

* リモートでゲームを遊ぶ
* ヘッドレス環境でカメラのプレビューを表示して画角調整

== 課題

=== Raspiban jessie liteにxを後から導入した環境&Uubntu MATE環境ではうまく動かなかった

* 切り分けをして原因を調べたい．

=== 他のsystemでも動くか試したい

* Raspberry Pi上のUbuntu MATE / Dragonboard上のDebian等
